//
//  AppDelegate.swift
//  SimpleCloud
//
//  Created by SungJae Lee on 2016. 11. 21..
//  Copyright © 2016년 SungJae Lee. All rights reserved.
//

import UIKit
import CloudKit
import UserNotifications

class WebServiceSettings: NSObject
{
    var serviceAPIKey: String?
    var serviceURL: String?
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let webServiceSetting = WebServiceSettings()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]){ (granted, error) in }
        UIApplication.shared.registerForRemoteNotifications()
        
        //updateWebServiceSettings()
        subscribeToWebServiceSettingsChanges()
        return true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let cloudKitNotification = CKNotification.init(fromRemoteNotificationDictionary: userInfo as! [String : NSObject])
        if cloudKitNotification.notificationType == CKNotificationType.query {
            let recordID = (cloudKitNotification as! CKQueryNotification).recordID
            
            if recordID?.recordName == "9c2c7817-6030-4e2b-8111-f0a1b37d9949" {
                updateWebServiceSettings()
            }
        }
    }
    
    private func subscribeToWebServiceSettingsChanges() {
        let subscribed = UserDefaults.standard.bool(forKey: "subscribedToUpdates")
        if subscribed == false {
            let publicDatabase = CKContainer.default().publicCloudDatabase
            let recordID = CKRecordID(recordName: "9c2c7817-6030-4e2b-8111-f0a1b37d9949")
            publicDatabase.fetch(withRecordID: recordID) { (record, error) in
                guard error == nil else {
                    //Handle error here
                    print(error!)
                    return
                }
               
                // dictionaryWithValuesForKeys , setValuesForKeys.
                if let localRecord = record {
                    let dic = localRecord.dictionaryWithValues(forKeys: ["serviceURL","serviceAPIKey"])
                    self.webServiceSetting.setValuesForKeys(dic)
                    print("urlString : \(self.webServiceSetting.serviceURL!) , apiKey: \(self.webServiceSetting.serviceAPIKey!)")
                    print("Start Subscription..............")
                    CKContainer.default().accountStatus {
                        (accountStatus,error) in
                            guard error == nil else {
                                // Handle error here
                                print(error!)
                                return
                            }
                        
                        if accountStatus == .available {
                    
                                let predicate = NSPredicate(format: "TRUEPREDICATE")
                                let subscription = CKQuerySubscription(recordType: "WebServiceSettings", predicate: predicate, options: .firesOnRecordUpdate)
                            
                                let notiInfo = CKNotificationInfo()
                                notiInfo.alertBody = "Subscription 푸쉬 테스트......!!!!"
                                notiInfo.shouldBadge = true
                                subscription.notificationInfo = notiInfo
                            
                                publicDatabase.save(subscription) { (subscription,error) in
                                    guard error == nil else {
                                        // Handle error here
                                        print(error!)
                                        return
                                    }
                                    UserDefaults.standard.set(true, forKey: "subscribedToUpdates")
                                    UserDefaults.standard.synchronize()
                                }
                            }
                        }
                }
            }
        }
    }
    
    private func updateWebServiceSettings() {
        let publicDatabase = CKContainer.default().publicCloudDatabase
        let recordID = CKRecordID(recordName: "9c2c7817-6030-4e2b-8111-f0a1b37d9949")
        publicDatabase.fetch(withRecordID: recordID) { (record,error) in
            guard error == nil else {
                print(error!)
                return
            }
            
            // dictionaryWithValuesForKeys , setValuesForKeys.
            if let localRecord = record {
                let dic = localRecord.dictionaryWithValues(forKeys: ["serviceURL","serviceAPIKey"])
                self.webServiceSetting.setValuesForKeys(dic)
                print("urlString : \(self.webServiceSetting.serviceURL!) , apiKey: \(self.webServiceSetting.serviceAPIKey!)")
                
            }
            
            // Update the local copy of the settings
            //            if let localRecord = record,
            //                let urlString = localRecord["serviceURL"] as? String,
            //                let apiKey = localRecord["serviceAPIKey"] as? String {
            //               // self.updateSettingsWithServiceURL(NSURL(string: urlString)!, serviceApiKey:apiKey )
            //                print("urlString : \(urlString) , apiKey: \(apiKey)")
            //            }
        }
    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        resetBadgeCounter()
    }
    
    private func resetBadgeCounter() {
        let badgeResetOperation = CKModifyBadgeOperation(badgeValue: 0)
        badgeResetOperation.modifyBadgeCompletionBlock = { (error) -> Void in
            if error != nil {
                print("Error resetting badge: \(error)")
            }
            else {
                UIApplication.shared.applicationIconBadgeNumber = 0
            }
        }
        CKContainer.default().add(badgeResetOperation)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

extension AppDelegate: UNUserNotificationCenterDelegate
{
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound])
    }
    

}

